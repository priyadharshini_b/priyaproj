import java.io.*;
import java.util.*;

public class HealthInsurance {
	public static int basePremium=5000;
	public static int reducedAMount=0;
	
	//Calculating age premium
	public int ageRule(int age){
		int tot=0;
		if((age>=18 && age<=25))
		{
			tot=(basePremium*10)/100;
		}
		if((age>=25 && age<=30)){
			tot=(basePremium*(10+10))/100;
		}
		if((age>=30 && age<=35)){
			tot=(basePremium*(10+10+10))/100;
		}
		else if(age>=40)
		{
			tot=(basePremium*(10+10+10+20))/100;
		}
			
		return tot;
	}
	
	//Calculating gender premium
	public int genderRule(String gender){
		int tot=0;
		if(("male").equals(gender)){
			tot=(basePremium*2)/100;
		}
		return tot;
	}
	
	//Calculating current health premium
	public int currenthealthRule(boolean hypertenion,boolean bp,boolean sugar,boolean overweight){
		int tot=0;
		List healthlist=new ArrayList();
		if(hypertenion){healthlist.add(hypertenion);}
		if(bp){healthlist.add(bp);}
		if(sugar){healthlist.add(sugar);}
		if(overweight){healthlist.add(overweight);}
		if(healthlist.size()!=0){
			tot=(basePremium*(healthlist.size()))/100;
			
		}
		return tot;
	}
	
	//Calculating current habits rule premium
	public int habitsRule(boolean smoking,boolean alcohol,boolean exercise,boolean drugs){
		int tot=0;
		int reduce=0;
		List badHabitlist=new ArrayList();
		List gudHabitlist=new ArrayList();
		if(exercise){reducedAMount=((basePremium*3)/100);}
		if(smoking){badHabitlist.add(smoking);}
		if(alcohol){badHabitlist.add(alcohol);}
		if(drugs){badHabitlist.add(drugs);}
		if(badHabitlist.size()!=0){
			tot=(basePremium*(badHabitlist.size())*3)/100;
		}
		
		return tot;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
System.out.println("After passing norman gomes status");
int age=34;
String gender="male";
boolean hypertenion=false;
boolean bp=false;
boolean sugar=false;
boolean overweight=true;
boolean smoking=false;
boolean alcohol=true;
boolean exercise=true;
boolean drugs=false;
int agepremium;
int genderpremium;
int currentHealthPremium;
int habitsPremium;
HealthInsurance h=new HealthInsurance();

System.out.println("calling agerule");
agepremium=h.ageRule(age);
System.out.println("agepremium "+agepremium);
System.out.println("calling genderrule");
genderpremium=h.genderRule(gender);
System.out.println("genderpremium "+genderpremium);
System.out.println("calling currenthealthRule");
currentHealthPremium= h.currenthealthRule(hypertenion,bp,sugar,overweight);
System.out.println("currentHealthPremium "+currentHealthPremium);
System.out.println("calling habitsRule");
habitsPremium=h.habitsRule(smoking,alcohol,exercise,drugs);
System.out.println("BadhabitsPremium "+habitsPremium);
System.out.println("ReducedAmount for gud habit : "+reducedAMount);

int totPremium=basePremium+agepremium+genderpremium+currentHealthPremium+habitsPremium-reducedAMount;
System.out.println("totPremium: "+totPremium);

	}

}
